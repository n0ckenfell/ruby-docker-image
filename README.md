# Ruby Docker Image by iBoard.cc

**This is work in progress, please stay tuned**

## Usage

    docker pull iboard/ruby-docker-image
    docker run iboard/ruby-docker-image

This pulls the docker-imager from docker hub and runs a simple spec.

## Changelog and Progress

**2015-04-24** - Installs ruby, rspec, and runs a simple test just to
 show our environment is ready for coding.

