require "spec_helper"

describe "A very basic test" do

  let(:message) { "Running in a docker image" }

  it "Runs in a docker image" do
    expect(message).to eq "Running in a docker image"
  end

end
